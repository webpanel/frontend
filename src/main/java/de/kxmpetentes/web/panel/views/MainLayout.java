package de.kxmpetentes.web.panel.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Nav;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.auth.AccessAnnotationChecker;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.security.AuthenticatedUser;
import de.kxmpetentes.web.panel.themes.ThemeVariant;
import de.kxmpetentes.web.panel.views.images.GalleryView;
import de.kxmpetentes.web.panel.views.settings.SettingsView;
import de.kxmpetentes.web.panel.views.shortenurls.ShortenUrlsView;
import de.kxmpetentes.web.panel.views.start.StartView;
import de.kxmpetentes.web.panel.views.users.UsersView;
import java.util.Optional;

public class MainLayout extends AppLayout {

  public static class MenuItemInfo extends ListItem {

    private final Class<? extends Component> view;

    public MenuItemInfo(String menuTitle, String iconClass, Class<? extends Component> view) {
      this.view = view;
      RouterLink link = new RouterLink();
      link.addClassNames("flex", "h-m", "items-center", "px-s", "relative", "text-secondary");
      link.setRoute(view);

      Span text = new Span(getTranslation(menuTitle));
      text.addClassNames("font-medium", "text-m", "whitespace-nowrap");

      link.add(new LineAwesomeIcon(iconClass), text);
      add(link);
    }

    public Class<?> getView() {
      return view;
    }

    @NpmPackage(value = "line-awesome", version = "1.3.0")
    public static class LineAwesomeIcon extends Span {

      public LineAwesomeIcon(String lineawesomeClassnames) {
        addClassNames("me-s", "text-xl");
        if (!lineawesomeClassnames.isEmpty()) {
          addClassNames(lineawesomeClassnames);
        }
      }
    }

  }

  private final AuthenticatedUser authenticatedUser;
  private final AccessAnnotationChecker accessChecker;

  public MainLayout(AuthenticatedUser authenticatedUser, AccessAnnotationChecker accessChecker) {
    this.authenticatedUser = authenticatedUser;
    this.accessChecker = accessChecker;
    addToNavbar(createHeaderContent());
  }

  private Component createHeaderContent() {
    Header header = new Header();
    header.addClassNames("bg-base", "border-b", "border-contrast-10", "box-border", "flex", "flex-col", "w-full");

    Div layout = new Div();
    layout.addClassNames("flex", "h-xl", "items-center", "px-l");

    H1 appName = new H1("Web Panel");
    appName.addClickListener(event -> getUI().ifPresent(ui -> ui.navigate(StartView.class)));
    appName.addClassNames("my-0", "me-auto", "text-xl", "cursor-pointer");
    layout.add(appName);

    Optional<User> maybeUser = authenticatedUser.get();
    if (maybeUser.isPresent()) {
      User user = maybeUser.get();

      Avatar avatar = new Avatar(user.getName());
      avatar.addClassNames("me-s");

      ContextMenu userMenu = new ContextMenu(avatar);
      userMenu.setOpenOnClick(true);
      userMenu.addItem(getTranslation("view.loginView.logout"), e -> authenticatedUser.logout());

      Span name = new Span(user.getName());
      name.addClassNames("font-medium", "text-m", "text-secondary");

      ThemeVariant themeVariant = ThemeVariant.byName(user.getUserSettings().getThemeName(), ThemeVariant.DEFAULT);
      UI.getCurrent().switchTheme(themeVariant.getThemeAttribute());

      layout.add(avatar, name);
    } else {
      Anchor loginLink = new Anchor("login", "Sign in");
      layout.add(loginLink);
    }

    Nav nav = new Nav();
    nav.addClassNames("flex", "gap-m", "overflow-auto", "px-m");

    UnorderedList list = new UnorderedList();
    list.addClassNames("flex", "list-none", "m-0", "p-0");
    nav.add(list);

    for (MenuItemInfo menuItem : createMenuItems()) {
      if (accessChecker.hasAccess(menuItem.getView())) {
        list.add(menuItem);
      }
    }

    header.add(layout, nav);
    return header;
  }

  private MenuItemInfo[] createMenuItems() {
    return new MenuItemInfo[]{
        new MenuItemInfo("constants.name.home", "la la-home", StartView.class),
        new MenuItemInfo("constants.name.gallery", "la la-images", GalleryView.class),
        new MenuItemInfo("constants.name.shortenUrls", "la la-globe", ShortenUrlsView.class),
        new MenuItemInfo("constants.name.users", "la la-users", UsersView.class),
        new MenuItemInfo("constants.name.settings", "la la-user", SettingsView.class),
    };
  }

}
