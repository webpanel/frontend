package de.kxmpetentes.web.panel.views.images;

import de.kxmpetentes.web.panel.core.entity.UploadedImage;

public record CachedImage(UploadedImage uploadedImage, String imageTitle, String imageViewsString,
                          String uploaderString, String imageUrl) {

}