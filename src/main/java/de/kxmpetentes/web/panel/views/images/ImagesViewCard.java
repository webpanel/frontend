package de.kxmpetentes.web.panel.views.images;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import de.kxmpetentes.web.panel.core.entity.UploadedImage;
import java.text.DateFormat;

public class ImagesViewCard extends ListItem {

  private final DateFormat dateFormat;
  private final String backendServerUrl;
  private final UploadedImage currentImage;

  public ImagesViewCard(ImageManager imageManager, UploadedImage currentImage) {
    this.dateFormat = imageManager.getDateFormat();
    this.currentImage = currentImage;
    this.backendServerUrl = imageManager.getBackendServerUrl();
  }

  public void createUi() {
    addClassNames("bg-contrast-5", "flex", "flex-col", "items-start", "p-m", "rounded-l");
    addClickListener(event -> getUI().ifPresent(ui -> ui.getPage().open(currentImage.getImageId(), "_blank")));

    String imageUrl = String.format("%s/image/%s", backendServerUrl, currentImage.getFileName());

    Div imageHolder = new Div();
    imageHolder.addClassNames("flex", "items-center", "justify-center", "mb-m", "overflow-hidden", "rounded-s", "w-full");
    imageHolder.setHeight("160px");
    Image image = new Image(imageUrl, imageUrl);
    image.addClassNames("list-image");
    imageHolder.add(image);

    Span header = new Span();
    header.addClassNames("text-xl", "font-semibold");
    header.setText(currentImage.getFileName());

    Span subtitle = new Span();
    subtitle.addClassNames("text-s", "text-secondary");
    subtitle.setText(dateFormat.format(currentImage.getUploadTime()));

    String imageViews = String.format(getLocale(), "» Views: %,d", currentImage.getViews());

    Paragraph description = new Paragraph();
    description.addClassName("my-m");

    Span badge = new Span();
    badge.getElement().setAttribute("theme", "badge");
    badge.setText(imageViews);

    add(imageHolder, header, subtitle, description, badge);
  }
}
