package de.kxmpetentes.web.panel.views.images;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Main;
import com.vaadin.flow.component.html.OrderedList;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import de.kxmpetentes.web.panel.core.entity.UploadedImage;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.security.AuthenticatedUser;
import de.kxmpetentes.web.panel.views.MainLayout;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Gallery")
@RolesAllowed("USER")
@Route(value = "gallery", layout = MainLayout.class)
public class GalleryView extends Main implements HasComponents, HasStyle {

  private OrderedList imageContainer;

  private final ImageManager imageManager;
  private final AuthenticatedUser authenticatedUser;

  @Autowired
  public GalleryView(ImageManager imageManager, AuthenticatedUser authenticatedUser) {
    this.imageManager = imageManager;
    this.authenticatedUser = authenticatedUser;
  }

  @PostConstruct
  private void constructUi() {
    addClassNames("images-view", "max-w-screen-lg", "mx-auto", "pb-l", "px-l");

    HorizontalLayout container = new HorizontalLayout();
    container.addClassNames("items-center", "justify-between");

    VerticalLayout headerContainer = new VerticalLayout();
    H2 header = new H2(getTranslation("view.galleryView.title"));
    header.addClassNames("mb-0", "mt-l", "text-2xl");
    Paragraph description = new Paragraph(getTranslation("view.galleryView.description"));
    description.addClassNames("mb-xl", "mt-0", "text-secondary");
    headerContainer.add(header, description);

    imageContainer = new OrderedList();
    imageContainer.addClassNames("gap-m", "grid", "list-none", "m-0", "p-0");

    container.add(headerContainer);
    add(container, imageContainer);

    addImages();
  }

  private void addImages() {
    Optional<User> optionalUser = authenticatedUser.get();
    if (optionalUser.isEmpty()) {
      return;
    }

    User user = optionalUser.get();
    for (UploadedImage image : imageManager.getImageService().getImagesByUploader(user.getName())) {
      ImagesViewCard card = new ImagesViewCard(imageManager, image);
      card.createUi();
      imageContainer.add(card);
    }
  }
}