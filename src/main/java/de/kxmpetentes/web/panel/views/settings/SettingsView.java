package de.kxmpetentes.web.panel.views.settings;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinServletRequest;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.core.entity.UserSetting;
import de.kxmpetentes.web.panel.core.service.UserService;
import de.kxmpetentes.web.panel.security.AuthenticatedUser;
import de.kxmpetentes.web.panel.themes.ThemeVariant;
import de.kxmpetentes.web.panel.util.PathUtils;
import de.kxmpetentes.web.panel.util.Validator;
import de.kxmpetentes.web.panel.views.MainLayout;
import de.kxmpetentes.web.panel.views.images.ImageManager;
import io.sentry.Sentry;
import java.io.ByteArrayInputStream;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import org.hibernate.validator.constraints.Length;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

@PageTitle("Settings")
@Route(value = "settings", layout = MainLayout.class)
@RolesAllowed("USER")
public class SettingsView extends VerticalLayout implements LocaleChangeObserver, BeforeLeaveObserver {

  private UserSetting userSetting;
  private boolean userChangedSetting = false;

  //Account Settings
  @Length(min = 3)
  private final TextField usernameField = new TextField();
  @Length(min = 8)
  private final PasswordField passwordField = new PasswordField();
  private final Button saveUserdataButton = new Button();

  //Appearance Settings
  private final Select<Locale> localeSelect = new Select<>();
  private final Select<ThemeVariant> themeSelect = new Select<>();

  //Image Uploader Settings
  private final Dialog downloadDialog = new Dialog();
  private final TextField pageTitleField = new TextField();
  private final TextField hexColorField = new TextField();
  private final Button downloadConfigurationButton = new Button();
  private final Button saveImageSettings = new Button();

  private final User user;
  private final ImageManager imageManager;
  private final AuthenticatedUser authenticatedUser;
  private final UserService userService;
  private final I18NProvider i18NProvider;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public SettingsView(AuthenticatedUser authenticatedUser, ImageManager imageManager, UserService userService,
      I18NProvider i18NProvider, PasswordEncoder passwordEncoder) {
    this.user = authenticatedUser.get().orElse(null);
    this.authenticatedUser = authenticatedUser;
    this.imageManager = imageManager;
    this.userService = userService;
    this.i18NProvider = i18NProvider;
    this.passwordEncoder = passwordEncoder;
  }

  @PostConstruct
  private void createUi() {
    if (user != null) {
      userSetting = user.getUserSettings();
    }

    //Account Settings
    Div userSettingsContainer = new Div();
    userSettingsContainer.addClassNames("user-settings-container");
    userSettingsContainer.add(usernameField, passwordField, saveUserdataButton);
    usernameField.setLabel(getTranslation("view.settingsView.changeUsername"));
    usernameField.addClassName("username-field");
    passwordField.setLabel(getTranslation("view.settingsView.changePassword"));
    passwordField.addClassName("password-field");
    saveUserdataButton.setText(getTranslation("view.settingsView.saveUserdata"));
    saveUserdataButton.addClassName("save-userdata-button");
    add(userSettingsContainer);

    //Appearance Settings
    Div languageSettingsContainer = new Div(localeSelect, themeSelect);
    languageSettingsContainer.addClassName("language-settings-container");
    localeSelect.addClassName("language-selection");
    localeSelect.setLabel(getTranslation("view.settingsView.localeSelect"));
    themeSelect.addClassName("theme-selection");
    themeSelect.setLabel(getTranslation("view.settingsView.changeTheme"));
    add(languageSettingsContainer);

    //Image Uploader Settings
    Div imageSettingsContainer = new Div();
    imageSettingsContainer.addClassName("image-settings-container");
    imageSettingsContainer.add(downloadDialog, pageTitleField, hexColorField, downloadConfigurationButton,
        saveImageSettings);
    pageTitleField.setLabel(getTranslation("view.settingsView.changePageTitle"));
    pageTitleField.addClassName("page-title-field");
    hexColorField.setLabel(getTranslation("view.settingsView.changeHexColor"));
    hexColorField.addClassName("hex-color-field");
    downloadConfigurationButton.setText(getTranslation("view.settingsView.downloadShareXConfig"));
    downloadConfigurationButton.addClassName("download-button");
    saveImageSettings.setText(getTranslation("view.settingsView.saveImageSettings"));
    saveImageSettings.addClassName("image-settings-button");
    add(imageSettingsContainer);

    configureUi();
  }

  private void configureUi() {
    setAlignItems(Alignment.START);
    //Account Settings
    usernameField.setPlaceholder(user.getUsername());
    usernameField.addThemeVariants(TextFieldVariant.MATERIAL_ALWAYS_FLOAT_LABEL);
    passwordField.addThemeVariants(TextFieldVariant.MATERIAL_ALWAYS_FLOAT_LABEL);
    saveUserdataButton.addThemeVariants(ButtonVariant.MATERIAL_CONTAINED);
    saveUserdataButton.addClickListener(this::saveUserdataChanges);

    //Language Settings
    localeSelect.setItems(i18NProvider.getProvidedLocales());
    localeSelect.setTextRenderer(Locale::getDisplayLanguage);
    localeSelect.setValue(user.getUserSettings().getLanguage());
    localeSelect.addValueChangeListener(this::changeLanguageAction);
    themeSelect.setItems(ThemeVariant.values());
    themeSelect.setTextRenderer(ThemeVariant::getThemeName);
    themeSelect.setValue(ThemeVariant.byName(user.getUserSettings().getThemeName(), ThemeVariant.DEFAULT));
    themeSelect.addValueChangeListener(this::changeTheme);

    //Image Uploader Settings
    downloadDialog.setDraggable(true);
    downloadDialog.setCloseOnOutsideClick(true);
    downloadDialog.setCloseOnEsc(true);
    downloadDialog.addDialogCloseActionListener(event -> {
      downloadDialog.removeAll();
      downloadDialog.close();
    });
    downloadDialog.setHeaderTitle(getTranslation("view.settingsView.downloadShareXConfig"));
    pageTitleField.setPlaceholder(userSetting.getUploaderPageTitle());
    pageTitleField.addThemeVariants(TextFieldVariant.MATERIAL_ALWAYS_FLOAT_LABEL);
    pageTitleField.addValueChangeListener(this::changePageTitle);
    hexColorField.addThemeVariants(TextFieldVariant.MATERIAL_ALWAYS_FLOAT_LABEL);
    hexColorField.setPlaceholder(userSetting.getHexColorCode());
    hexColorField.addValueChangeListener(this::changeHexColor);

    downloadConfigurationButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    downloadConfigurationButton.addClickListener(this::downloadConfig);
    saveImageSettings.addThemeVariants(ButtonVariant.LUMO_SUCCESS);
    saveImageSettings.addClickListener(this::saveImageSettings);
  }

  private void saveUserdataChanges(ClickEvent<Button> event) {
    userChangedSetting = false;
    if (usernameField.isEmpty() && passwordField.isEmpty()) {
      return;
    }

    if (!usernameField.isEmpty()) {
      user.setUsername(usernameField.getValue());
    }

    if (!passwordField.isEmpty()) {
      user.setHashedPassword(passwordEncoder.encode(passwordField.getValue()));
    }

    userService.update(user);
    Notification.show(getTranslation("view.settingsView.updatedUserdata"));
    authenticatedUser.logout();
  }

  private void changeLanguageAction(ComponentValueChangeEvent<Select<Locale>, Locale> event) {
    userChangedSetting = true;
    Locale language = event.getValue();
    UI.getCurrent().setLocale(language);
    userSetting.setLanguage(language);
  }

  private void changeTheme(ComponentValueChangeEvent<Select<ThemeVariant>, ThemeVariant> event) {
    userChangedSetting = true;
    ThemeVariant themeVariant = event.getValue();
    userSetting.setThemeName(themeVariant.getThemeName());
    UI.getCurrent().switchTheme(themeVariant.getThemeAttribute());
  }

  private void downloadConfig(ClickEvent<Button> event) {
    try {
      JSONObject shareXConfig = new JSONObject()
          .put("Version", "14.0.1")
          .put("Name", "ImageUploader Web Panel")
          .put("DestinationType", "ImageUploader")
          .put("RequestMethod", "POST")
          .put("RequestURL", imageManager.getBackendServerUrl() + "/upload/image")
          .put("Headers", new JSONObject()
              .put("token", user.getApiToken().toString()))
          .put("Body", "MultipartFormData")
          .put("FileFormName", "MultipartFormData")
          .put("URL", PathUtils.getServerUrl(VaadinServletRequest.getCurrent()) + "/{json:imageId}");

      String shareXConfigString = shareXConfig.toString(2);
      Anchor downloadAnchor = new Anchor(new StreamResource("web-panel-image.sxcu", () ->
          new ByteArrayInputStream(shareXConfigString.getBytes())),
          getTranslation("view.settingsView.clickToDownload"));
      downloadDialog.add(downloadAnchor);
    } catch (JSONException e) {
      Sentry.captureException(e);
      Notification.show("Could not generate ShareX Config");
    }
    downloadDialog.open();
  }

  private void changePageTitle(ComponentValueChangeEvent<TextField, String> event) {
    userChangedSetting = true;
    String pageTitle = event.getValue();
    userSetting.setUploaderPageTitle(pageTitle);
  }

  private void changeHexColor(ComponentValueChangeEvent<TextField, String> event) {
    if (!Validator.isHexColor(event.getValue())) {
      hexColorField.setErrorMessage("Not a valid hex code");
      return;
    }

    userChangedSetting = true;
    userSetting.setHexColorCode(event.getValue());
  }

  private void saveImageSettings(ClickEvent<Button> event) {
    if (!userChangedSetting) {
      return;
    }

    userService.update(user);
  }

  @Override
  public void beforeLeave(BeforeLeaveEvent event) {
    if (user != null && userChangedSetting) {
      Notification.show(getTranslation("view.settingsView.settingsAutoSaved"));
      userService.update(user);
    }
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    usernameField.setLabel(getTranslation("view.settingsView.changeUsername"));
    passwordField.setLabel(getTranslation("view.settingsView.changePassword"));
    saveUserdataButton.setText(getTranslation("view.settingsView.saveUserdata"));

    localeSelect.setLabel(getTranslation("view.settingsView.localeSelect"));
    themeSelect.setLabel(getTranslation("view.settingsView.changeTheme"));

    pageTitleField.setLabel(getTranslation("view.settingsView.changePageTitle"));
    hexColorField.setLabel(getTranslation("view.settingsView.changeHexColor"));
    downloadConfigurationButton.setText(getTranslation("view.settingsView.downloadShareXConfig"));
    saveImageSettings.setText(getTranslation("view.settingsView.saveImageSettings"));
  }
}
