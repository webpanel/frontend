package de.kxmpetentes.web.panel.views.shortenurls;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.data.VaadinSpringDataHelpers;
import de.kxmpetentes.web.panel.core.Role;
import de.kxmpetentes.web.panel.core.entity.ShortenUrl;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.core.service.ShortenUrlService;
import de.kxmpetentes.web.panel.security.AuthenticatedUser;
import de.kxmpetentes.web.panel.util.Validator;
import de.kxmpetentes.web.panel.views.MainLayout;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

@RolesAllowed("USER")
@PageTitle("Shorten Urls")
@Route(value = "shorten-urls/:shortenUrlID?/:action?(edit)", layout = MainLayout.class)
public class ShortenUrlsView extends Div implements BeforeEnterObserver {

  private static final String SHORTEN_URL_EDIT_ROUTE_TEMPLATE = "shorten-urls/%s/edit";

  private final Button save = new Button();
  private final Button cancel = new Button();
  private final Button delete = new Button();
  private final Grid<ShortenUrl> grid = new Grid<>(ShortenUrl.class, false);
  private final BeanValidationBinder<ShortenUrl> binder = new BeanValidationBinder<>(ShortenUrl.class);

  private final ShortenUrlService shortenUrlService;
  private final AuthenticatedUser authenticatedUser;

  @URL
  private TextField originalUrl;
  @Length(min = 3)
  private TextField redirectUrl;

  private ShortenUrl currentShortenUrl;

  @Autowired
  public ShortenUrlsView(ShortenUrlService shortenUrlService, AuthenticatedUser authenticatedUser) {
    this.shortenUrlService = shortenUrlService;
    this.authenticatedUser = authenticatedUser;
  }

  @PostConstruct
  private void createUi() {
    addClassNames("shorten-urls-view");

    // Create UI
    SplitLayout splitLayout = new SplitLayout();
    createGridLayout(splitLayout);
    createEditorLayout(splitLayout);
    add(splitLayout);

    configureUi();
  }

  private void configureUi() {
    // Configure Grid
    grid.addClassNames("rounded-l");
    grid.addColumn("originalUrl").setAutoWidth(true)
        .setHeader(getTranslation("view.shortenUrlsView.originalUrl"));
    grid.addColumn("redirectUrl").setAutoWidth(true)
        .setHeader(getTranslation("view.shortenUrlsView.redirectKeyWord"));
    grid.setItems(query -> shortenUrlService.list(PageRequest.of(query.getPage(), query.getPageSize(),
        VaadinSpringDataHelpers.toSpringDataSort(query))).stream());
    grid.addThemeVariants(GridVariant.MATERIAL_COLUMN_DIVIDERS);
    grid.asSingleSelect().addValueChangeListener(event -> {
      if (event.getValue() != null) {
        UI.getCurrent().navigate(String.format(SHORTEN_URL_EDIT_ROUTE_TEMPLATE, event.getValue().getId()));
      } else {
        clearForm();
        UI.getCurrent().navigate(ShortenUrlsView.class);
      }
    });

    configureButtons();

    binder.bindInstanceFields(this);
  }

  private void configureButtons() {
    Optional<User> user = authenticatedUser.get();
    if (user.isEmpty() || !user.get().hasRole(Role.URL_EDITOR)) {
      save.setEnabled(false);
      cancel.setEnabled(false);
      delete.setEnabled(false);
      return;
    }

    save.setText(getTranslation("view.shortenUrlsView.saveButton"));
    save.addClickListener(this::saveAction);
    cancel.setText(getTranslation("view.shortenUrlsView.cancelButton"));
    cancel.addClickListener(this::cancelAction);
    delete.setText(getTranslation("view.shortenUrlsView.deleteButton"));
    delete.addClickListener(this::deleteAction);
  }

  private void createEditorLayout(SplitLayout splitLayout) {
    Div editorLayoutDiv = new Div();
    editorLayoutDiv.setClassName("editor-layout");

    Div editorDiv = new Div();
    editorDiv.setClassName("editor");
    editorLayoutDiv.add(editorDiv);

    FormLayout formLayout = new FormLayout();
    originalUrl = new TextField(getTranslation("view.shortenUrlsView.originalUrl"));
    redirectUrl = new TextField(getTranslation("view.shortenUrlsView.redirectKeyWord"));
    Component[] fields = new Component[]{originalUrl, redirectUrl};

    formLayout.add(fields);
    editorDiv.add(formLayout);
    createButtonLayout(editorLayoutDiv);

    splitLayout.addToSecondary(editorLayoutDiv);
  }

  private void createButtonLayout(Div editorLayoutDiv) {
    HorizontalLayout buttonLayout = new HorizontalLayout();
    buttonLayout.setClassName("button-layout");
    cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
    buttonLayout.add(save, cancel, delete);
    editorLayoutDiv.add(buttonLayout);
  }

  private void createGridLayout(SplitLayout splitLayout) {
    Div wrapper = new Div();
    wrapper.setClassName("grid-wrapper");
    splitLayout.addToPrimary(wrapper);
    wrapper.add(grid);
  }

  private void saveAction(ClickEvent<Button> event) {
    if (Validator.containsSpaces(redirectUrl.getValue(), "Redirect key") ||
        Validator.containsSpaces(originalUrl.getValue(), "URL")) {
      return;
    }

    if (redirectUrl.getValue().contains(" ")) {
      Notification.show("Redirect Key should not contain spaces");
      return;
    }

    if (currentShortenUrl == null) {
      currentShortenUrl = new ShortenUrl();
    }

    try {
      binder.writeBean(this.currentShortenUrl);
    } catch (ValidationException e) {
      Notification.show("An exception happened while trying to store the shortenUrl details.");
    }

    shortenUrlService.update(this.currentShortenUrl);
    clearForm();
    refreshGrid();
    Notification.show(getTranslation("view.shortenUrlsView.updateNotification"));
    UI.getCurrent().navigate(ShortenUrlsView.class);
  }

  private void cancelAction(ClickEvent<Button> event) {
    clearForm();
    refreshGrid();
  }

  private void deleteAction(ClickEvent<Button> event) {
    if (this.currentShortenUrl == null) {
      return;
    }

    shortenUrlService.delete(this.currentShortenUrl.getId());
    clearForm();
    refreshGrid();
    Notification.show(getTranslation("view.shortenUrlsView.removedNotification"));
    UI.getCurrent().navigate(ShortenUrlsView.class);
  }

  @Override
  public void beforeEnter(BeforeEnterEvent event) {
    Optional<UUID> shortenUrlId = event.getRouteParameters().get("shortenUrlID").map(UUID::fromString);
    if (shortenUrlId.isEmpty()) {
      return;
    }

    Optional<ShortenUrl> shortenUrlFromBackend = shortenUrlService.get(shortenUrlId.get());
    if (shortenUrlFromBackend.isPresent()) {
      populateForm(shortenUrlFromBackend.get());
    } else {
      Notification.show(String.format("The requested shortenUrl was not found, ID = %s", shortenUrlId.get()),
          3000, Notification.Position.BOTTOM_START);
      refreshGrid();
      event.forwardTo(ShortenUrlsView.class);
    }
  }

  private void refreshGrid() {
    grid.select(null);
    grid.getLazyDataView().refreshAll();
  }

  private void clearForm() {
    populateForm(null);
  }

  private void populateForm(ShortenUrl value) {
    this.currentShortenUrl = value;
    binder.readBean(this.currentShortenUrl);
  }

}
