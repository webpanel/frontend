package de.kxmpetentes.web.panel.views.images;

import com.vaadin.flow.server.communication.IndexHtmlRequestHandler;
import de.kxmpetentes.web.panel.core.service.UploadedImageService;
import de.kxmpetentes.web.panel.core.service.UserService;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageManager {

  private final DateFormat dateFormat = DateFormat.getInstance();
  private final Map<String, CachedImage> cachedImages = new HashMap<>();
  private final ExecutorService executorService = Executors.newSingleThreadExecutor();
  private final String backendServerUrl = System.getenv("BACKEND_SERVER_URL") == null ?
      "http://localhost:8081" : System.getenv("BACKEND_SERVER_URL");

  private final UploadedImageService imageService;

  @Autowired
  public ImageManager(UploadedImageService imageService) {
    this.imageService = imageService;
  }

  @PostConstruct
  public void addDefaultRequestHandler() {
    IndexHtmlRequestHandler.addRequestHandler(new UploadedImageDisplay(this));
  }

  public DateFormat getDateFormat() {
    return dateFormat;
  }

  public ExecutorService getExecutorService() {
    return executorService;
  }

  public Map<String, CachedImage> getCachedImages() {
    return cachedImages;
  }

  public String getBackendServerUrl() {
    return backendServerUrl;
  }

  public UploadedImageService getImageService() {
    return imageService;
  }

}
