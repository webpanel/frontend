package de.kxmpetentes.web.panel.views.images;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Main;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.vaadin.flow.server.communication.DynamicMetadataListener;
import de.kxmpetentes.web.panel.core.entity.UploadedImage;
import de.kxmpetentes.web.panel.core.service.UploadedImageService;
import de.kxmpetentes.web.panel.core.service.UserService;
import de.kxmpetentes.web.panel.util.PathUtils;
import de.kxmpetentes.web.panel.views.MainLayout;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;

@AnonymousAllowed
@Route(value = ":imageId", layout = MainLayout.class)
public class UploadedImageDisplay extends Main implements BeforeEnterObserver, DynamicMetadataListener {

  private static final char[] INVISIBLE_SPACES = new char[]{
      '\u200c',
      '\u200d',
      '\u200e',
      '\u200f',
  };

  private final HorizontalLayout container = new HorizontalLayout();

  private final ImageManager imageManager;
  private final UploadedImageService imageService;

  @Autowired
  public UploadedImageDisplay(ImageManager imageManager) {
    this.imageManager = imageManager;
    this.imageService = imageManager.getImageService();
  }

  @PostConstruct
  private void createUi() {
    addClassNames("max-w-screen-lg", "mx-auto");
    add(container);
    container.addClassNames("items-center", "justify-center", "max-w-screen-lg", "pb-s", "pg-s");
  }

  @Override
  public void updateDocument(Document document, VaadinRequest request) {
    boolean startsWithInvisibleSpaces = false;
    for (char invisibleSpace : INVISIBLE_SPACES) {
      if (startsWithInvisibleSpaces) {
        break;
      }

      startsWithInvisibleSpaces = request.getPathInfo().startsWith(String.valueOf(invisibleSpace));
    }

    if (startsWithInvisibleSpaces) {
      return;
    }

    String restPath = PathUtils.splitPath(request, "/");
    UploadedImage currentImage = imageService.getImageByImageId(restPath);
    if (currentImage == null) {
      return;
    }

    Locale locale = getLocale();
    String pageTitle = currentImage.getDomain();
    String imageViewsString = String.format(locale, "» Views: %,d", currentImage.getViews());
    String uploaderString = String.format(locale, "» Uploaded by: %s", currentImage.getUploader());
    String imageUrl = String.format("%s/image/%s", imageManager.getBackendServerUrl(), currentImage.getFileName());

    imageManager.getCachedImages().put(restPath,
        new CachedImage(currentImage, pageTitle, imageViewsString, uploaderString, imageUrl));

    document.title(pageTitle);

    Elements metaTags = document.getElementsByTag("meta");
    for (Element metaTag : metaTags) {
      String name;
      if (metaTag.hasAttr("name")) {
        name = metaTag.attr("name");
      } else {
        name = metaTag.attr("property");
      }

      switch (name.toLowerCase()) {
        case "theme-color" -> metaTag.attr("content", currentImage.getHexColor());
        case "og:site_name" -> metaTag.attr("content", pageTitle);
        case "og:title" -> metaTag.attr("content", String.format(locale,
            "%s | %s", uploaderString, imageViewsString));
        case "og:image" -> metaTag.attr("content", imageUrl);
        default -> { /* Skip */ }
      }
    }
  }

  @Override
  public void beforeEnter(BeforeEnterEvent event) {
    Optional<String> parameter = event.getRouteParameters().get("imageId");
    if (parameter.isEmpty()) {
      return;
    }

    Locale locale = getLocale();
    String imageId = URLDecoder.decode(parameter.get(), StandardCharsets.UTF_8);
    CachedImage image = imageManager.getCachedImages().get(imageId);
    if (image == null) {
      return;
    }

    Image uploadedImage = new Image(image.imageUrl(), "Uploaded Image");
    uploadedImage.addClassNames("image-placeholder");
    container.add(uploadedImage);

    //Add image info to ui
    Paragraph uploadInfo = new Paragraph(String.format(locale, "» Uploaded on: %s | %s",
        imageManager.getDateFormat().format(image.uploadedImage().getUploadTime()), image.uploaderString()));
    Paragraph imageViews = new Paragraph(image.imageViewsString());
    Div uploadInfoContainer = new Div(uploadInfo, imageViews);
    uploadInfoContainer.addClassNames("centered-text", "upload-info-container");
    add(uploadInfoContainer);

    //Update image views
    image.uploadedImage().incrementViews();
    imageManager.getExecutorService().execute(() -> imageService.insertOrUpdate(image.uploadedImage()));
  }

}
