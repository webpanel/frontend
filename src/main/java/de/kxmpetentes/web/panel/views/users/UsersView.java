package de.kxmpetentes.web.panel.views.users;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.data.VaadinSpringDataHelpers;
import de.kxmpetentes.web.panel.core.Role;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.core.service.UserService;
import de.kxmpetentes.web.panel.views.MainLayout;
import io.sentry.Sentry;
import java.util.EnumSet;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;


@Uses(Icon.class)
@PageTitle("Users")
@RolesAllowed("ADMIN")
@Route(value = "users/:userId?/:action?(edit)", layout = MainLayout.class)
public class UsersView extends Div implements BeforeEnterObserver {

  private static final String USER_EDIT_ROUTE_TEMPLATE = "users/%s/edit";

  private final Button saveButton = new Button();
  private final Button cancelButton = new Button();
  private final Button deleteButton = new Button();
  private final Grid<User> grid = new Grid<>(User.class, false);
  private final BeanValidationBinder<User> binder = new BeanValidationBinder<>(User.class);

  private final UserService userService;
  private final PasswordEncoder passwordEncoder;

  @Length(min = 3)
  private TextField username;
  @Length(min = 3)
  private TextField name;
  @Length(min = 8)
  private PasswordField passwordField;
  private Checkbox adminCheckbox;
  private Checkbox urlEditorCheckbox;

  private User user;

  @Autowired
  public UsersView(UserService userService, PasswordEncoder passwordEncoder) {
    this.userService = userService;
    this.passwordEncoder = passwordEncoder;
  }

  @PostConstruct
  private void createUi() {
    addClassNames("users-view");
    SplitLayout splitLayout = new SplitLayout();
    createGridLayout(splitLayout);
    createEditorLayout(splitLayout);
    add(splitLayout);

    configureUi();
  }

  private void configureUi() {
    grid.addColumn("username").setAutoWidth(true)
        .setHeader(getTranslation("constants.name.username"));
    grid.addColumn("name").setAutoWidth(true)
        .setHeader(getTranslation("constants.name.name"));
    grid.setItems(query -> userService.list(PageRequest.of(query.getPage(), query.getPageSize(),
        VaadinSpringDataHelpers.toSpringDataSort(query))).stream());
    grid.addThemeVariants(GridVariant.MATERIAL_COLUMN_DIVIDERS);
    grid.asSingleSelect().addValueChangeListener(event -> {
      if (event.getValue() == null) {
        clearForm();
        UI.getCurrent().navigate(UsersView.class);
        return;
      }

      UI.getCurrent().navigate(String.format(USER_EDIT_ROUTE_TEMPLATE, event.getValue().getId()));
    });

    saveButton.setText(getTranslation("view.userView.saveButton"));
    cancelButton.setText(getTranslation("view.userView.cancelButton"));
    deleteButton.setText(getTranslation("view.userView.deleteButton"));
    saveButton.addClickListener(this::saveAction);
    cancelButton.addClickListener(this::cancelAction);
    deleteButton.addClickListener(this::deleteAction);

    binder.bindInstanceFields(this);
  }

  private void createEditorLayout(SplitLayout splitLayout) {
    Div editorLayoutDiv = new Div();
    editorLayoutDiv.setClassName("editor-layout");

    Div editorDiv = new Div();
    editorDiv.setClassName("editor");
    editorLayoutDiv.add(editorDiv);

    FormLayout formLayout = new FormLayout();
    username = new TextField(getTranslation("constants.name.username"));
    name = new TextField(getTranslation("constants.name.name"));
    passwordField = new PasswordField(getTranslation("constants.name.password"));
    urlEditorCheckbox = new Checkbox(getTranslation("constants.name.url_editor"));
    adminCheckbox = new Checkbox(getTranslation("constants.name.admin"));

    formLayout.add(username, name, passwordField, urlEditorCheckbox, adminCheckbox);
    editorDiv.add(formLayout);
    createButtonLayout(editorLayoutDiv);

    splitLayout.addToSecondary(editorLayoutDiv);
  }

  private void createButtonLayout(Div editorLayoutDiv) {
    HorizontalLayout buttonLayout = new HorizontalLayout();
    buttonLayout.setClassName("button-layout");
    saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    cancelButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
    deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
    buttonLayout.add(saveButton, cancelButton, deleteButton);
    editorLayoutDiv.add(buttonLayout);
  }

  private void createGridLayout(SplitLayout splitLayout) {
    Div wrapper = new Div();
    wrapper.setClassName("grid-wrapper");
    splitLayout.addToPrimary(wrapper);
    wrapper.add(grid);
  }

  private void saveAction(ClickEvent<Button> event) {
    if (user == null) {
      user = new User();
      user.setApiToken(UUID.randomUUID());
    }

    try {
      binder.writeBean(user);
    } catch (ValidationException e) {
      Notification.show("An exception happened while trying to store the user details.");
      Sentry.captureException(e);
    }

    user.setUsername(username.getValue());
    user.setName(name.getValue());
    user.setRoles(adminCheckbox.getValue() ? EnumSet.allOf(Role.class) : EnumSet.of(Role.USER));
    if (urlEditorCheckbox.getValue()) {
      user.addRole(Role.URL_EDITOR);
    }

    if (!passwordField.isEmpty()) {
      user.setHashedPassword(passwordEncoder.encode(passwordField.getValue()));
    }

    userService.update(user);

    clearForm();
    refreshGrid();

    Notification.show(getTranslation("view.userView.updatedUser"));
    UI.getCurrent().navigate(UsersView.class);
  }

  private void cancelAction(ClickEvent<Button> event) {
    clearForm();
    refreshGrid();
  }

  private void deleteAction(ClickEvent<Button> event) {
    if (user == null) {
      return;
    }

    userService.delete(user.getId());

    clearForm();
    refreshGrid();

    Notification.show(getTranslation("view.userView.removedUser"));
    UI.getCurrent().navigate(UsersView.class);
  }

  @Override
  public void beforeEnter(BeforeEnterEvent event) {
    Optional<UUID> userId = event.getRouteParameters().get("userId").map(UUID::fromString);
    if (userId.isEmpty()) {
      return;
    }

    Optional<User> userFromBackend = userService.get(userId.get());
    if (userFromBackend.isPresent()) {
      populateForm(userFromBackend.get());
    } else {
      Notification.show(getTranslation("view.userView.userNotFound"), 3000, Notification.Position.BOTTOM_START);
      refreshGrid();
      event.forwardTo(UsersView.class);
    }
  }

  private void refreshGrid() {
    grid.select(null);
    grid.getLazyDataView().refreshAll();
  }

  private void clearForm() {
    populateForm(null);
  }

  private void populateForm(User value) {
    this.user = value;
    if (value == null) {
      urlEditorCheckbox.setValue(false);
      adminCheckbox.setValue(false);
      passwordField.clear();
    } else {
      adminCheckbox.setValue(value.getRoles().contains(Role.ADMIN));
      urlEditorCheckbox.setValue(value.getRoles().contains(Role.URL_EDITOR));
    }

    grid.select(user);
    binder.readBean(user);
  }

}