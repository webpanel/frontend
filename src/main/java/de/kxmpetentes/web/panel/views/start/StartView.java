package de.kxmpetentes.web.panel.views.start;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.core.service.ShortenUrlService;
import de.kxmpetentes.web.panel.core.service.UploadedImageService;
import de.kxmpetentes.web.panel.core.service.UserService;
import de.kxmpetentes.web.panel.security.AuthenticatedUser;
import de.kxmpetentes.web.panel.views.MainLayout;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Start")
@RolesAllowed("USER")
@Route(value = "", layout = MainLayout.class)
@RouteAlias(value = "home", layout = MainLayout.class)
public class StartView extends HorizontalLayout {

  private final NumberField userCount = new NumberField();
  private final NumberField imagesCount = new NumberField();
  private final NumberField shortenUrlsCount = new NumberField();
  private final NumberField imagesByUploaderCount = new NumberField();

  private final UserService userService;
  private final UploadedImageService imageService;
  private final ShortenUrlService shortenUrlService;

  private final AuthenticatedUser user;

  @Autowired
  public StartView(AuthenticatedUser user, UserService userService, UploadedImageService imageService,
      ShortenUrlService shortenUrlService) {
    this.user = user;
    this.userService = userService;
    this.imageService = imageService;
    this.shortenUrlService = shortenUrlService;
  }

  @PostConstruct
  private void createUi() {
    setVerticalComponentAlignment(Alignment.END);
    add(userCount, imagesCount, shortenUrlsCount, imagesByUploaderCount);
    addClassNames("start-view");

    configureUi();
  }

  private void configureUi() {
    Div statisticDiv = new Div();
    statisticDiv.addClassNames("justify-between", "mx-auto", "statistics-container");

    userCount.setLabel(getTranslation("view.startView.userAccountsDisplay"));
    userCount.setValue((double) userService.count());
    userCount.setReadOnly(true);
    userCount.addClassNames("statistics-field");

    imagesCount.setLabel(getTranslation("view.startView.uploadedImageAmount"));
    imagesCount.setValue((double) imageService.count());
    imagesCount.setReadOnly(true);
    imagesCount.addClassNames("statistics-field");

    shortenUrlsCount.setLabel(getTranslation("view.startView.shortenUrlsAmount"));
    shortenUrlsCount.setValue((double) shortenUrlService.count());
    shortenUrlsCount.setReadOnly(true);
    shortenUrlsCount.addClassNames("statistics-field");

    statisticDiv.add(userCount, imagesCount, shortenUrlsCount);
    Optional<User> optionalUser = user.get();
    if (optionalUser.isPresent()) {
      imagesByUploaderCount.setLabel(getTranslation("view.startView.uploadedByUser"));
      imagesByUploaderCount.setValue((double) imageService.countByUploader(optionalUser.get().getName()));
      imagesByUploaderCount.setReadOnly(true);
      imagesByUploaderCount.addClassNames("statistics-field");
      statisticDiv.add(imagesByUploaderCount);
    }

    add(statisticDiv);
  }


}
