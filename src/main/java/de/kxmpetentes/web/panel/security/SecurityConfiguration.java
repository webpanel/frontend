package de.kxmpetentes.web.panel.security;

import com.vaadin.flow.spring.security.VaadinWebSecurityConfigurerAdapter;
import de.kxmpetentes.web.panel.views.login.LoginView;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends VaadinWebSecurityConfigurerAdapter {

  public static final String LOGOUT_URL = "/";

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    super.configure(http);
    setLoginView(http, LoginView.class, LOGOUT_URL);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    super.configure(web);
  }
}
