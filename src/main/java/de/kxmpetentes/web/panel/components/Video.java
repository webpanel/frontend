package de.kxmpetentes.web.panel.components;

import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.PropertyDescriptor;
import com.vaadin.flow.component.PropertyDescriptors;
import com.vaadin.flow.component.Tag;

@Tag("video")
public class Video extends HtmlContainer {

  private static final PropertyDescriptor<String, String> srcDescriptor = PropertyDescriptors
      .attributeWithDefault("src", "");

  public Video(String src) {
    setSrc(src);
    getElement().setProperty("controls", true);
  }

  public void setSrc(String src) {
    set(srcDescriptor, src);
  }
}
