package de.kxmpetentes.web.panel.i18n;

import com.vaadin.flow.i18n.I18NProvider;
import io.sentry.Sentry;
import io.sentry.SentryEvent;
import io.sentry.protocol.Message;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;


@Component
public class SimpleI18NProvider implements I18NProvider {

  public static final Locale ENGLISH = new Locale("en");
  public static final Locale GERMAN = new Locale("de");
  public static final Locale SPANISH = new Locale("es");

  private Map<String, ResourceBundle> localeMap;

  @PostConstruct
  private void initMap() {
    localeMap = new HashMap<>();

    for (final Locale locale : getProvidedLocales()) {
      final ResourceBundle resourceBundle = ResourceBundle.getBundle("translations", locale);
      localeMap.put(locale.getLanguage(), resourceBundle);
    }
  }

  @Override
  public List<Locale> getProvidedLocales() {
    return List.of(ENGLISH, GERMAN, SPANISH);
  }

  @Override
  public String getTranslation(String key, Locale locale, Object... params) {
    String rawString = null;
    try {
      rawString = localeMap.get(locale.getLanguage()).getString(key);
      return MessageFormat.format(rawString, params);
    } catch (final MissingResourceException e) {
      SentryEvent event = new SentryEvent();
      Message message = new Message();
      message.setMessage("No translation found for key " + key + " %n");
      event.setMessage(message);
      Sentry.captureEvent(event);

      return String.format("!{%s}", key);
    } catch (final IllegalArgumentException e) {
      Sentry.captureException(e);
      return rawString;
    }
  }

}
