package de.kxmpetentes.web.panel.i18n;

import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.UIInitEvent;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.core.service.UserRepository;
import java.util.Locale;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringComponent
public class ServiceInitListener implements VaadinServiceInitListener {

  private final I18NProvider i18NProvider;
  private final UserRepository userRepository;

  public ServiceInitListener(I18NProvider i18NProvider, UserRepository userRepository) {
    this.i18NProvider = i18NProvider;
    this.userRepository = userRepository;
  }

  @Override
  public void serviceInit(ServiceInitEvent serviceInitEvent) {
    serviceInitEvent.getSource().addUIInitListener(this::loadLanguage);
  }

  private void loadLanguage(UIInitEvent event) {
    SecurityContext context = SecurityContextHolder.getContext();
    String name = context.getAuthentication().getName();
    User user = userRepository.findByUsername(name);
    if (user != null) {
      event.getUI().setLocale(user.getUserSettings().getLanguage());
      return;
    }

    Locale locale = VaadinService.getCurrentRequest().getLocale();
    if (i18NProvider.getProvidedLocales().contains(locale)) {
      event.getUI().setLocale(locale);
    } else {
      event.getUI().setLocale(i18NProvider.getProvidedLocales().get(0));
    }

  }

}
