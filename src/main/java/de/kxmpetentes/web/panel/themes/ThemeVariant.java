package de.kxmpetentes.web.panel.themes;

public enum ThemeVariant {

  DEFAULT(new Theme("Dark", "dark")),
  CATPPUCCIN_MOCHA_YELLOW(new Theme("Catppuccin Mocha Yellow", "dark", "catppuccin-mocha mocha-yellow")),
  CATPPUCCIN_MOCHA_SAPPHIRE(new Theme("Catppuccin Mocha Sapphire", "dark", "catppuccin-mocha mocha-sapphire")),
  CATPPUCCIN_MACCHIATO_LAVENDER(new Theme("Catppuccin Macchiato Lavender", "dark",
      "catppuccin-macchiato macchiato-lavender"));

  private final Theme theme;

  ThemeVariant(Theme theme) {
    this.theme = theme;
  }

  public Theme getTheme() {
    return theme;
  }

  public String getThemeName() {
    return theme.getThemeName();
  }

  public String getThemeAttribute() {
    return theme.getThemeAttribute();
  }

  public static ThemeVariant byName(String themeName, ThemeVariant defaultTheme) {
    for (ThemeVariant value : values()) {
      if (value.getThemeName().equals(themeName)) {
        return value;
      }
    }

    return defaultTheme;
  }
}
