package de.kxmpetentes.web.panel.themes;

public class Theme {

  private final String themeName;
  private final String lumoVariant;
  private final String themeAttribute;

  public Theme(String themeName, String lumoVariant) {
    this(themeName, lumoVariant, "");
  }

  public Theme(String themeName, String lumoVariant, String themeAttribute) {
    this.themeName = themeName;
    this.lumoVariant = lumoVariant;
    this.themeAttribute = themeAttribute;
  }

  public String getThemeAttribute() {
    return lumoVariant + " " + themeAttribute;
  }

  public String getThemeName() {
    return themeName;
  }
}
