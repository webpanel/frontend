package de.kxmpetentes.web.panel.util;

import com.vaadin.flow.component.notification.Notification;
import java.util.regex.Pattern;

public final class Validator {

  private Validator() {
  }

  private static final Pattern HEX_COLOR_PATTERN = Pattern.compile("^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$");

  public static boolean isHexColor(String value) {
    return HEX_COLOR_PATTERN.matcher(value).matches();
  }

  public static boolean containsSpaces(String string, String name) {
    if (string.contains(" ")) {
      Notification.show(name + " should not contain spaces.");
      return true;
    }

    return false;
  }


}
