package de.kxmpetentes.web.panel.util;

import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinServletRequest;
import javax.servlet.http.HttpServletRequest;

public final class PathUtils {

  private PathUtils() {
  }

  public static String splitPath(VaadinRequest request, String path) {
    return request.getPathInfo().replaceFirst(path, "");
  }

  public static String getServerUrl(VaadinServletRequest request) {
    HttpServletRequest servletRequest = request.getHttpServletRequest();
    StringBuilder urlBuilder = new StringBuilder(servletRequest.getScheme());
    urlBuilder.append("://").append(servletRequest.getServerName());
    if (request.getServerPort() != 80 && request.getServerPort() != 443) {
      urlBuilder.append(":").append(request.getServerPort());
    }

    return urlBuilder.toString();
  }

}
