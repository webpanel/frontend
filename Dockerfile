FROM openjdk:18-alpine
WORKDIR /app
COPY frontend-1.9.0.jar /app
CMD ["java", "-jar", "frontend-1.9.0.jar"]