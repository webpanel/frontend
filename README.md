# Web Panel

## Deploying to Production

To create a production build, call `./gradle clean build -Pproduction`.
This will build a JAR file with all the dependencies and front-end resources,
ready to be deployed. The file can be found in the `build/libs/` folder after the build completes.

Once the JAR file is built, you can run it using
`java -jar build/libs/frontend-version.jar`

Or you can create a Docker image using the given Dockerfile.

The logo was created by [Nueng_wana on FlatIcon](https://www.flaticon.com/de/autoren/nueng-wana)
